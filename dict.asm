%include "lib.inc"

global find_word

find_word:
push r8
push r9
mov r8, rdi
mov r9, rsi
.loop:
    test r9, r9
    je .err
    mov rdi, r8
    lea rsi, [r9 + 8]
    call string_equals
    cmp rax, 1
    je .found
    mov r9, [r9]
    jmp .loop
.err:
   xor rax, rax
   jmp .exit
.found:
   mov rax, r9
.exit:
   pop r9
   pop r8
   ret
