%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define buf_size 255
%define ERR 2
%define SYSCALL_WRITE 1

section .rodata
  lengthErr: db "Buffer overflowed!", 0
  notFoundErr: db "Key not found!", 0


section .bss
    buf: resb buf_size


global _start
section .text
_start:
  mov rdi, buf
  mov rsi, buf_size
  call read_str
  test rax, rax
  je .length_err
  mov rdi, rax
  mov rsi, first_word
  call find_word
  test rax, rax
  jz .not_found
  lea rdi, [rax+8]
  push rdi
  call string_length
  pop rdi
  lea rdi, [rdi+rax+1]
  call print_string
  ret
.length_err:
    mov rdi, lengthErr
    jmp print_err
.not_found:
    mov rdi, notFoundErr
    jmp print_err

read_str:
    sub rsp, 48
    mov [rsp], rdi
    mov [rsp + 16], rsi
    xor rcx, rcx
    mov [rsp + 32], rcx
    .loop:
         call read_char
         mov rdi, [rsp]
         mov rsi, [rsp + 16]
         mov rcx, [rsp + 32]
         cmp rax, 0xA
         je .end_str
         cmp rax, 0
         je .end_str
         cmp rcx, rsi
         je .end_buffer
         mov [rdi + rcx], rax
         inc rcx
         mov [rsp + 32], rcx
         jmp .loop
    .end_buffer:
         xor rax, rax
         jmp .end
    .end_str:
         xor byte[rdi + rcx], 0
         mov rax, rdi
    .end:
        add rsp, 48
        ret
    

print_err:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, ERR
    syscall
    ret



