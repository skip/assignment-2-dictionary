ASM = nasm -f elf64
RM = rm -f
all: main.o lib.o dict.o
	@echo "Programm has been build!"
	@ld -o $@ $^

%.o: %.asm $(wildcard *.inc)
	@$(ASM) $<
	@echo "Object file $@ has been build!"
clean:
	@echo "Cleaning..."
	@$(RM) *.o

.PHONY:all clean
