import subprocess

inputs = ["web-programmirovanie","fasfafs","PL", "fksajlksjfl", "aboba"*200,"Osnovi proffesionalnoi deyatelnosti"]
outputs = ["WP", "","Programmings languages", "", "", "OPD"]
errors = [ "", "Key not found!","", "Key not found!", "Buffer overflowed!","" ]
fails = []
for i in range(len(inputs)):
    process = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    output, error = process.communicate(input=inputs[i].encode())
    output = output.decode().strip()
    error = error.decode().strip()
    if output == outputs[i] and error == errors[i]:
        print(".", end="")
    else:
        print("F", end="")
        fails.append([i, inputs[i], output, outputs[i], error, errors[i]])
if (len(fails)==0):
    res = " OK"
else:
    res = ""
print(res)
for fail in fails:
    print("----------------------------")
    print("Fault when exeuting test " + str(fail[0]) + " with \"" + fail[1] + "\" parameters.")
    print("Expected: (strout) \"" + fail[3] + "\" , recieved \"" + fail[2] + "\"")
    print("          (strerr) \"" + fail[5] + "\" , recieved \"" + fail[4] + "\"")
